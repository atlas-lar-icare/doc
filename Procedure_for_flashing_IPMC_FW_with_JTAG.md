<div align="center">
  <center><h1>Procedure for flashing IPMC F/W with JTAG</h1></center><br><br>

  ![DRAFT](share/images/DRAFT.png){width=40%}
</div><br><br>

## Prerequits
### Hardware

- IPMC-LAPP devkit with an IPMC mezzanine
- USB 2.0 Mini-B Male for power suply
- Olimex ARM-USB-OCD-H an USB to JTAG debugger
- LAr machine with access to ICARE framework and IPMC software
- Personal laptop or LAr machine to run openOCD server

### Software
 - OS Distribution: Windows 10 or above and/or AlmaLinux 9 and/or CentOS 7
 - openOCD version 0.11 or above
 - ICARE release 00-06-01
 - LArC-IPMC version 1.4.2

### Test bench setup
> :triangular_flag_on_post: **Important**
>
> The Olimex ARM-USB-OCD-H must be connected via the JTAG connector to the devkit and to your laptop or LAr Machine via a USB Type B to Type A cable. The IPMC-LAPP mezzanine must be plugged into the devkit. A second USB type A to mini-B cable is required to power the devkit.

## 1. Start the openOCD server 

> :information_source: **Note**
>
> In this example, we are running the openOCD server on a LAr machine.

![start the openOCD server](share/images/openOCD.gif)

## 2. Setting up ICARE and LArC-IPMC environments

![ICARE and LArC-IPMC environment](share/images/LArC-IPMC_environment.gif)

## 3. Set openOCD server IP address

> :information_source: **Note**
>
> In this example, as the openOCD server runs on the same machine, we'll use `localhost` and we'll uncomment the following line in the `<...>/LArC-IPMC/LArC/cmt/requirements` file:
>
> ```makefile
> macro openocd "localhost"
> ```

![Set openOCD IP address](share/images/set_openOCD_ip_address.gif)



## 4. Flashing IPMC F/W and checking the version

![Flashing IPMC F/W with JTAG](share/images/LArC-IPMC_flashing_FW.gif)
