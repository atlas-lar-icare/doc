%----------------------------------------------------------------
% SPI Package
%----------------------------------------------------------------

%----------------------------------------------------------------
\subsection{SPI Multi-Master Feature}

\subsubsection{Objectives}

The \gls{IPMC} and \gls{IOIF} \glspl{MCU} communicate via a \gls{SPI} link. As message exchange should not be blocking, both \glspl{MCU} are defined as Master on the bus. 

Hence, when a \gls{MCU} wants to send a message, it takes control over the bus and does not wait for an answer immediately. If no answer is received after a given timeout, the message is resent until a maximum number of retries has been reached. In this case, the receiver is then considered as disconnected.\\

The goal of this feature is to provide the \gls{IPMC} and the \gls{IOIF} \glspl{MCU} with a way to automatically manage conflicting accesses on the \gls{SPI} bus. To do so, relying on the Multi-Master topology, each \gls{MCU} uses a GPIO pin configured as an output to control the hardware NSS pin of the other one, that is configured as an input pin \textit{(See Figure \ref{hw_nss_multimaster})}:
\begin{itemize}
	\item By default, both \gls{MCU}s are in \textbf{Slave} mode.
	\item When one of the  \gls{MCU}s wants to communicate, it goes into \textbf{Master} mode to temporarily take control of the bus.
	\item If its \textbf{NSS} pin is \textbf{low}, it means that the other \gls{MCU} is already trying to take control of the bus. In this case, an interrupt is generated, and the \gls{MCU} goes into \textbf{MODe Fault \textit{(MODF)} state} and return to \textbf{Slave} mode.
	\item Otherwise, when the communication is completed, the \textbf{Master} \gls{MCU} releases the bus by setting the \textbf{Slave} \gls{MCU}'s \textbf{NSS} pin \textbf{high}.
\end{itemize}

\begin{figure}
\begin{center}
	\includegraphics[scale=0.5]{src/LEGACY/img/hw_nss_multimaster.pdf}
	\caption{Hardware NSS feature - Multi-Master mode}
	\label{hw_nss_multimaster}
\end{center}
\end{figure}

In addition, as the communication is always uni-directional, we only use one wire to send and receive data \textit{(MOSI)}. The \textit{MISO} pin is thus used to notify the receiver that a new frame transmission has begun (\textit{'frame start'} on rising edge, and \textit{'frame stop'} on falling edge).\\


%----------------------------------------------------------------
\subsubsection{Software Architecture}

The SPI library has been remodeled in order to support the Multi-Master mode. The use case representing the data transmission is depicted in \textit{Figure \ref{spi_mm_use_case_data_transmission}}:
\begin{itemize}
	\item The user can register two callbacks: one called when a requested transmission has been completed (\textbf{Register Tx Done Callback}), the other one called when a full data frame has been received (\textbf{Register Rx Done Callback}).
	\item The library manages two internal buffers: one used to transmit data, the other for reception. The TX buffer is copied from the given user buffer in the \textbf{Write Buffer} case. The RX buffer is directly used by the library to store incoming data packets. It is provided to the user in the \textbf{Rx Done Callback} case. The user should then copy the content of this buffer on its own, as it is released after exiting the callback.
	\item Before sending data, the buffer size and the MODe Fault status are checked (\textbf{Check Buffer Size} and \textbf{Check Bus Conflict}).
\end{itemize}

\begin{figure}
\begin{center}
	\includegraphics[scale=0.5]{src/LEGACY/img/diagrams/spi_mm_use_case_data_transmission.pdf}
	\caption{SPI Multi-Master mode - Data Transmission Use-Case}
	\label{spi_mm_use_case_data_transmission}
\end{center}
\end{figure}

The class diagram representing the library API is depicted in the \textit{Figure \ref{spi_mm_api}}, showing the user's public interface one hand, and the private implementation details on the other hand.

\begin{figure}
\begin{center}
	\includegraphics[scale=0.65]{src/LEGACY/img/diagrams/spi_mm_api.pdf}
	\caption{SPI Multi-Master mode - Class Diagram - Library API}
	\label{spi_mm_api}
\end{center}
\end{figure}

An error callback is also defined, to handle communication failures: it provides to the user with the status of the communication, to decipher whether a message should be retransmitted or not.\\


%----------------------------------------------------------------
\subsubsection{Tests Suite}

A test is defined as a scenario involving a transmitter and a receiver. For each test function, the two cases are described and a feature or event is tested. Tests are executed sequentially. Because some tests can cause an \gls{MCU} to be reseted, it is thus necessary to re-synchronize the \glspl{MCU} sequences.

The synchronization is done as follow. At the beginning of each test, the IPMC sends its test number and then waits for the IOIF. The IOIF waits the test number of the IPMC, then sends its own:
\begin{itemize}
	\item If (own\_nb > other\_nb) $\rightarrow$ Wait for another number
	\item If (own\_nb < other\_nb) $\rightarrow$ Skip the test
	\item If (own\_nb == other\_nb) $\rightarrow$ Run the test\\
\end{itemize}

\textit{Figure \ref{spi_mm_test_sync_fsm}} depicts the state machines run by each \gls{MCU} to synchronize the test sequence.

\begin{center}
\begin{figure}
	\includegraphics[scale=0.5]{src/LEGACY/img/diagrams/spi_mm_test_sync_fsm.pdf}
	\caption{SPI Multi-Master mode - Test Synchronization FSM}
	\label{spi_mm_test_sync_fsm}
\end{figure}
\end{center}


%----------------------------------------------------------------
\subsection{DMA Feature}

\subsubsection{Objectives}

The \gls{SPI} peripheral can be configured to use an internal \gls{DMA} engine. \gls{DMA} allows to directly transfer data from memory to peripheral registers and vice-versa, without involving the central processor unit. As shown in \textit{Figure \ref{stm32f4_dma_engines}}, each DMA engine can access a given set of peripherals via the AHB bus matrix.\\

\begin{figure}
\begin{center}
	\includegraphics[scale=0.6]{src/LEGACY/img/stm32f4_dma_engines.png}
	\caption{STM32F4 DMA Engines \cite{stm32_ref_manual}}
	\label{stm32f4_dma_engines}
\end{center}
\end{figure}

This feature enhances the transfer bandwidth, as the communication is no longuer dependant of the \gls{MCU} usage.\\


\subsubsection{Software Architecture}

For the \gls{SPI} peripheral, two \gls{DMA} streams can be set to manage the transfer:
\begin{itemize}
	\item The \textbf{stream 2} of \textbf{DMA2}, to silently move incoming data from the \gls{SPI} RX register to a buffer located in memory. Once the transfer is completed, an interruption is raised.
	\item The \textbf{stream 3} of \textbf{DMA2}, to directly transfer data from memory to the \gls{SPI} TX register. Once the transfer is completed, another interruption is raised.
\end{itemize}
