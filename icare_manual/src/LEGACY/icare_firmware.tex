%----------------------------------------------------------------
% ICARE Firmware
%----------------------------------------------------------------

The \gls{ICARE} firmware is the application running on top of the \gls{IPMC} mezzanine. It is compiled both for the \gls{IPMC} and the \gls{IOIF} \gls{MCU}s.

%----------------------------------------------------------------
\subsection{Environment Setup}

In addition to the information provided in the next subsections, the setup of the \gls{ICARE} environment and its configuration are described in the document written by Guy Perrot \cite{doc_install_prog_ipmc}. It introduces the installation of the tools and drivers for Windows 8.1, and details the commands to get a fresh local copy of the firmware repository.


%-----------------------------------------
\subsection{Working Directories}

\textbf{LAPP installation:}\\

When working at \textbf{\gls{LAPP}}, two folders need to be mounted in order to retrieve the source code and to be able to build the \gls{ICARE} firmware:
\begin{itemize}
	\item home1 or home3: this folder contains your user directory. It will be used to work on a local copy of the firmware sources
	\item software\_dev : this directory contains the source code and the tools to build the firmware
\end{itemize}

On Linux, if not already done, these folders can be mounted using \textit{sshfs}. Here are some aliases that can be pasted into your bash profile \textit{(replace "`username"' by your own user name)}:

\begin{lstlisting}[language=bash]
$ alias mount_home1='sshfs username@lappsl.in2p3.fr:/home1 /home1/'
$ alias mount_software='sshfs username@lappsl.in2p3.fr:/software_dev /software_dev'
\end{lstlisting}
\vspace{1.0em}

\noindent To unmount them, define and use the following aliases:
\begin{lstlisting}[language=bash]
$ alias unmount_home1='fusermount -u /home1'
$ alias unmount_software='fusermount -u /software_dev'
\end{lstlisting}
\vspace{1.0em}


\textbf{Local installation:}\\

When working outside the \gls{LAPP} network, the \gls{ICARE} firmware can be installed locally using the auto-extractable archive that can be download on the \gls{ICARE} website. The installation procedure can be found at the same location:\\

\url{http://lappwiki.in2p3.fr/twiki/bin/view/AtlasLapp/Informatique}


\subsection{OpenOCD}

In order to load the firmware on the \gls{IPMC} board, the following tools are required:
\begin{itemize}
	\item The ICARE environment previously installed
	\item The Olimex JTAG programmer with the OpenOCD software
\end{itemize}

The Olimex JTAG programmer can be used on SLC6, SLC7 or macOS 10.12+ with openocd-0.9.0 and later. On Windows 10, the Olimex drivers are working from the version \textbf{2.3} of the \textbf{Zadig} software. With previous version of Zadig \textit{(e.g. 2.2)}, one has to use the NGX JTAG interface instead.

The OpenOCD tool is available on the Linux machines at LAPP in the directory mentioned below. The connection with the board can be made using the following commands.\\

\begin{enumerate}
\item First, load the ICARE environment. At this time, the latest release is 00-02-00:
\begin{lstlisting}[language=bash]
$ source /software_dev/atlas/project/ICARE/releases/ICARE-00-02-00/admin/v0r6/cmt/setup.sh
\end{lstlisting}
\vspace{1.0em}

\item Then, go to the OpenOCD directory and connect to the board through the Olimex JTAG adapter:
\begin{lstlisting}[language=bash]
$ cd /software_dev/atlas/project/ICARE/
$ cd ./contrib/openocd/Linux/openocd-0.9.0-201505190955/scripts/
$ openocd -f interface/olimex-arm-usb-ocd-h.cfg -f target/ipmcv2_1.cfg
\end{lstlisting}
\vspace{1.0em}

It should display the following output:
\begin{lstlisting}[language=bash]
[laurent@localhost scripts]$ openocd -f interface/olimex-arm-usb-ocd-h.cfg -f target/ipmcv2_1.cfg
GNU ARM Eclipse 64-bits Open On-Chip Debugger 0.9.0-00073-gdd34716-dirty (2015-05-19-09:57)
Licensed under GNU GPL v2
For bug reports, read
	http://openocd.org/doc/doxygen/bugs.html
Info : only one transport option; autoselect 'jtag'
adapter speed: 1000 kHz
adapter_nsrst_delay: 100
cortex_m reset_config sysresetreq
Warn : Using DEPRECATED interface driver 'ft2232'
Info : Consider using the 'ftdi' interface driver, with configuration files in interface/ftdi/...
Info : max TCK change to: 30000 kHz
Info : clock speed 1000 kHz
Info : JTAG tap: ipmc.cpu tap/device found: 0x4ba00477 (mfg: 0x23b, part: 0xba00, ver: 0x4)
Info : JTAG tap: ipmc.bs tap/device found: 0x06413041 (mfg: 0x020, part: 0x6413, ver: 0x0)
Info : JTAG tap: cpld.cpld tap/device found: 0x020a20dd (mfg: 0x06e, part: 0x20a2, ver: 0x0)
Info : JTAG tap: ioif.cpu tap/device found: 0x4ba00477 (mfg: 0x23b, part: 0xba00, ver: 0x4)
Info : JTAG tap: ioif.bs tap/device found: 0x06413041 (mfg: 0x020, part: 0x6413, ver: 0x0)
Info : ipmc.cpu: hardware has 6 breakpoints, 4 watchpoints
Info : ioif.cpu: hardware has 6 breakpoints, 4 watchpoints
\end{lstlisting}
\vspace{1.0em}

\item Once the connection is done, you can launch a telnet connection to interact with the \gls{MCU}s thanks to OpenOCD or GDB commands:

\begin{lstlisting}[language=bash]
$ telnet localhost 4444
\end{lstlisting}
\vspace{1.0em}

The following output should be displayed:
\begin{lstlisting}[language=bash]
Trying ::1...
telnet: connect to address ::1: Connection refused
Trying 127.0.0.1...
Connected to localhost.
Escape character is '^]'.
Open On-Chip Debugger
> 
\end{lstlisting}
\vspace{1.0em}

Information about the two \glspl{MCU} can be printed using the 'targets' command:
\begin{lstlisting}[language=bash]
> targets
    TargetName         Type       Endian TapName            State
--  ------------------ ---------- ------ ------------------ ------------
 0  ipmc.cpu           cortex_m   little ipmc.cpu           running
 1* ioif.cpu           cortex_m   little ioif.cpu           running
>
\end{lstlisting}
\vspace{1.0em}

\end{enumerate}


\subsection{ICARE Source Code}

When working at \gls{LAPP}, the firmware source code can be fetched to a local directory using the following commands.\\

\noindent First, load the \gls{ICARE} environment:
\begin{lstlisting}[language=bash]
$ source /software_dev/atlas/project/ICARE/releases/\
ICARE-00-02-00/admin/v0r6/cmt/setup.sh
\end{lstlisting}
\vspace{1.0em}

\noindent Create a new local repository folder, for instance ICARE\_workarea:
\begin{lstlisting}[language=bash]
$ cd /home1/<username>/
$ mkdir ATLAS && cd ATLAS
$ cmt create_project ICARE_workarea
\end{lstlisting}
\vspace{1.0em}

\noindent Finally in project.cmt, add the following line setting the ICARE version you want to use:
\begin{lstlisting}[language=bash]
$ nano project.cmt
...
# Add the following line:
use releases ICARE-00-02-00
...
\end{lstlisting}
\vspace{1.0em}

\noindent Several aliases can be created to ease the load of the \gls{ICARE} environment:
\begin{lstlisting}[language=bash]
# ICARE aliases
alias source_icare='source /software_dev/atlas/project/ICARE/releases/ICARE-00-02-00/admin/v0r6/cmt/setup.sh'
alias cd2openocd='cd /software_dev/atlas/project/ICARE/contrib/openocd/Linux/openocd-0.9.0-201505190955/scripts/'
alias cd2icare_workarea='cd /home1/username/ATLAS/ICARE_workarea/'
\end{lstlisting}


\subsection{Package Development}

%-----------------------------------------
\textbf{Package compilation:}\\

\noindent By default, your local application will use the remote packages tagged in the SVN repository. If you want to configure a package with CMT or even make changes to the source code, you have to import it in your ICARE work area:
\begin{lstlisting}[language=bash]
$ cd /home1/username/ATLAS/ICARE_workarea/
\end{lstlisting}
\vspace{1.0em}

\noindent The version format for a package is v\textit{x}r\textit{y}p\textit{z}, with \textit{x, y, z} being respectively the version, release and patch numbers.
To get the SPI package along with the IMC, type the following command. It will import the spi and imc packages and will increment the revision version of each package \textit{('-i r' option)}. It is also possible to increment the version \textit{('-i v')} or the patch number \textit{('-i p')}:\\
\begin{lstlisting}[language=bash]
$ getpkg -i r spi imc
\end{lstlisting}
\vspace{1.0em}

\noindent Once you get the package, you can go to the SPI package CMT directory:
\begin{lstlisting}[language=bash]
$ cd spi/v0r5/cmt
\end{lstlisting}
\vspace{1.0em}

\noindent Use the command \textbf{"`make help"'} to get the list of the available commands associated with this packet. If any, the existing tests can be compiled with the command:
\begin{lstlisting}[language=bash]
$ make && make test
\end{lstlisting}
\vspace{1.0em}

\noindent The binary files can then be uploaded to the Flash using this command:
\begin{lstlisting}[language=bash]
$ make flash_spi_test_1 # in the case of the SPI package
\end{lstlisting}
\vspace{1.0em}

\noindent Finally, to circumvent manually removing / reinserting the carrier, it is possible to reset the \gls{MCU}s using the JTAG interface.\\
\textbf{Warning:} JTAG programming only works with Carrier v3 or newer.
\begin{lstlisting}[language=bash]
$ telnet localhost 4444
$ reset init
$ reset run
\end{lstlisting}
\vspace{1.0em}

%-----------------------------------------
\textbf{Sources commitment (SVN only):}\\

\noindent Before committing, ensure your local repository URL points to the \textbf{trunk} branche: 
\begin{lstlisting}[language=bash]
$ svn info
Path: .
URL: file:///software_dev/atlas/repo/ICARE/spi/trunk
Repository Root: file:///software_dev/atlas/repo/ICARE
Repository UUID: 5d6f7848-0351-41c7-bcaa-bd82fec97149
Revision: 661
Node Kind: directory
Schedule: normal
Last Changed Author: gantel
Last Changed Rev: 657
Last Changed Date: 2017-02-08 18:16:25 +0100 (Wed, 08 Feb 2017)
\end{lstlisting}
\vspace{1.0em}

\noindent Check if new files need to be added and the status of the modified files:
\begin{lstlisting}[language=bash]
$ svn status
\end{lstlisting}

\noindent To see the modifications in a file, use the following command:
\begin{lstlisting}[language=bash]
$ svn diff myFile
\end{lstlisting}
\vspace{1.0em}

\noindent You can install and configure colordiff to get a fancier output. On Linux SLC6:
\begin{lstlisting}[language=bash]
$ yum install colordiff
\end{lstlisting}

\noindent Then you can configure it as wanted:
\begin{lstlisting}[language=bash]
$ cp /etc/colordiffrc ~/.colordiffrc
$ nano ~/.colordiffrc
\end{lstlisting}

\noindent Choose the colors you want to apply for each type of output. Then configure SVN to use colordiff:
\begin{lstlisting}[language=bash]
$ nano ~/.subversion/config
\end{lstlisting}

\noindent This line allows to configure the editor:
\begin{lstlisting}[language=bash]
editor-cmd = sublime_text
\end{lstlisting}

\noindent This line is to choose the program used to perform the diff:
\begin{lstlisting}[language=bash]
diff-cmd = /usr/bin/colordiff
\end{lstlisting}
\vspace{1.0em}

\noindent If you are satisfied with the modifications you have done to the package, add the new files and commit:
\begin{lstlisting}[language=bash]
$ svn add myFile
$ svn commit -m "This is my new file" myFile
\end{lstlisting}

\noindent Finally, create a tag:
\begin{lstlisting}[language=bash]
$ cd spi/v0r5
$ svn update
$ svn cp . $SVNROOT/spi/tags/v0r5 -m "Add Multi-Master mode using hardware NSS"
$ svntags -lv spi
# Should be the version that we just commited
\end{lstlisting}


%-----------------------------------------
\subsection{Firmware Upgrade}

The firmware upgrade utility is necessary to update the firmware through Ethernet and should be compiled with the application. \textbf{If needed} re-compile it but it is already integrated to the 00-02-00 repository:
\begin{lstlisting}[language=bash]
$ cd fwUpgrade/v0r1p4/cmt
$ make
$ source setup.sh
$ cd ../../../inet/v0r1p2/cmt
$ make
$ make flash_inetsrvc
\end{lstlisting}
\vspace{1.0em}

\noindent Go to your application directory (demo in this example):
\begin{lstlisting}[language=bash]
$ cd ../../../demo/v0r1/cmt
$ make
$ source setup.sh
\end{lstlisting}
\vspace{1.0em}

\noindent If accessible, use the JTAG connection to get the IP address:
\begin{lstlisting}[language=bash]
$ make info
#CMT---> Info: Document mcu_info

===========================
IPMC - OTP area information
===========================
OpenOCD server: lappc-at27

MCU ID:         0x193c
PCB version:    v2.2
Serial Number:  35
---------------------------


===========================
IOIF - OTP area information
===========================
OpenOCD server: lappc-at27

MCU ID:         0x101f
PCB version:    v2.2
Serial Number:  35
MAC address:    00:22:8f:02:40:23
IP address:     134.158.98.183
---------------------------
\end{lstlisting}
\vspace{1.0em}

\noindent The IP address can also be retrieved sending a clia command to the shelf:
\lstset{
    escapeinside={(*@}{@*)},          % if you want to add LaTeX within your code
}
\begin{lstlisting}[language=bash]
$ clia board sendcmd 2e 07 2e a1 00
Pigeon Point Shelf Manager Command Line Interpreter

Completion code: 0x0 (0)
Response data: 2E A1 00 00 22 8F 02 40 6E (*@ \textbf{80 8D CA CB} @*) FF FF FF 00 80 8D CA 01 01 01 64
\end{lstlisting}
The address is stored in the bytes 10 to 13. For instance 80 8D CA CB becomes 128.141.202.203.
\vspace{1.0em}

\noindent Load the firmware in the memory, first the IPMC binary:
\begin{lstlisting}[language=bash]
$ fwu -t IPMC -n 134.158.98.183 -f ../arm-gcc47-dbg/bmc_IPMC.bin
+------------------------------------------------------+
|                ___ ___   _   ___ ___                 |
|               |_ _/ __| /_\ | _ \ __|                |
|                | | (__ / _ \|   / _|                 |
|               |___\___/_/ \_\_|_\___|                |
|                                                      |
| Intelligent plateform management Controller softwARE |
|                                                      |
|               Firmware upgrade utility.              |
|          Copyrigth (c) 2014-2015 LAPP/CNRS.          |
|                                                      |
+------------------------------------------------------+

Host       : 134.158.98.183
Port       : 5555
Target MCU : IPMC
Upgrade MCU: NO

Firmware Upgrade image : ../arm-gcc47-dbg/bmc_IPMC.bin (272.1KB)
Firmware image checksum: 9f60f71e
Downloading image 'bmc_IPMC.bin' ...OK
Download rate: 24.3KB/s 00:11.219
Programming CONFIGURATION flash memory ...OK
Programming 278652 bytes in CONFIGURATION flash memory successful.


$ fwu -t IPMC -n 134.158.98.183 -u
+------------------------------------------------------+
|                ___ ___   _   ___ ___                 |
|               |_ _/ __| /_\ | _ \ __|                |
|                | | (__ / _ \|   / _|                 |
|               |___\___/_/ \_\_|_\___|                |
|                                                      |
| Intelligent plateform management Controller softwARE |
|                                                      |
|               Firmware upgrade utility.              |
|          Copyrigth (c) 2014-2015 LAPP/CNRS.          |
|                                                      |
+------------------------------------------------------+

Host       : 134.158.98.183
Port       : 5555
Target MCU : IPMC
Upgrade MCU: YES

Send update command ...OK
Performing upgrade stage (take approximately 20 seconds).
\end{lstlisting}
\vspace{1.0em}

\noindent After waiting approximately 20-50 seconds depending on the size of the binary, repeat the operation for the IOIF binary:

\begin{lstlisting}[language=bash]

$ fwu -t IOIF -n 134.158.98.183 -f ../arm-gcc47-dbg/bmc_IOIF.bin
+------------------------------------------------------+
|                ___ ___   _   ___ ___                 |
|               |_ _/ __| /_\ | _ \ __|                |
|                | | (__ / _ \|   / _|                 |
|               |___\___/_/ \_\_|_\___|                |
|                                                      |
| Intelligent plateform management Controller softwARE |
|                                                      |
|               Firmware upgrade utility.              |
|          Copyrigth (c) 2014-2015 LAPP/CNRS.          |
|                                                      |
+------------------------------------------------------+

Host       : 134.158.98.183
Port       : 5555
Target MCU : IOIF
Upgrade MCU: NO

Firmware Upgrade image : ../arm-gcc47-dbg/bmc_IOIF.bin (226.9KB)
Firmware image checksum: 036fc318
Downloading image 'bmc_IOIF.bin' ...OK
Download rate: 23.7KB/s 00:09.573
Programming CONFIGURATION flash memory ...OK
Programming 232388 bytes in CONFIGURATION flash memory successful.
\end{lstlisting}
\vspace{1.0em}

\noindent Update the boot memory:
\begin{lstlisting}[language=bash]
$ fwu -t IOIF -n 134.158.98.183 -u
+------------------------------------------------------+
|                ___ ___   _   ___ ___                 |
|               |_ _/ __| /_\ | _ \ __|                |
|                | | (__ / _ \|   / _|                 |
|               |___\___/_/ \_\_|_\___|                |
|                                                      |
| Intelligent plateform management Controller softwARE |
|                                                      |
|               Firmware upgrade utility.              |
|          Copyrigth (c) 2014-2015 LAPP/CNRS.          |
|                                                      |
+------------------------------------------------------+

Host       : 134.158.98.183
Port       : 5555
Target MCU : IOIF
Upgrade MCU: YES

Send update command ...OK
Performing upgrade stage (take approximately 20 seconds).
\end{lstlisting}
\vspace{1.0em}

\noindent After the reboot of the IOIF which also takes 20-50 seconds, the IPMC will automatically reboot. Use the JTAG to check the new version currently running:
\begin{lstlisting}[language=bash]
[gantel@lappc-f533 cmt]$ make version
#CMT---> Info: Document mcu_version

===================================
IPMC - Version
===================================
OpenOCD server: lappc-at27

Release ver.: ICARE-00-02-00
Compilator  : (GNU) gcc 4.7.0 [dbg]
Binary image: bmc_IPMC.bin
Build ver.  : Jul 21 2017, 16:40:33
-----------------------------------


===================================
IOIF - Version
===================================
OpenOCD server: lappc-at27

Release ver.: ICARE-00-02-00
Compilator  : (GNU) gcc 4.7.0 [dbg]
Binary image: bmc_IOIF.bin
Build ver.  : Jul 21 2017, 16:40:33
-----------------------------------
\end{lstlisting}
\vspace{1.0em}
