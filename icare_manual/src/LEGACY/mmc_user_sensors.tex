%----------------------------------------------------------------
% User Sensors
%----------------------------------------------------------------

%----------------------------------------------------------------
Our \gls{MMC} user-code contains the FRU/\gls{SDR} information for the \gls{LATOME} board. It also maps the different sensors monitoring the board.\\

%----------------------------------------------------------------
\subsection{Sensors list}

\textit{Sensors.h} contains the list of sensors (and sub-sensors) of the \gls{LATOME} board, with their names, $I^2C$ addresses (and sub-addresses), conversion values and specific thresholds.

The \textit{Sensor} folder contains the files for each different sensors (initialization, reading part). Those files have been made with the tool given by the \gls{MMC} software, and implemented in the project. There are 2 different hardware sensors, managed by 3 different software components:
\begin{itemize}
	\item LM95234: Temperature sensor
	\item LTC2495: Current sensor
	\item LTC2495: Voltage sensor
\end{itemize}

LTC2495 is used either as a current or a voltage sensor. It requires an $I^2C$ request to initialize the conversion (160ms with normal configuration). So a specific configuration has been made to handle this issue using a structure with buffered values in LTC2495\_common.h for both current and voltage use-case. This allows sending a frame to initialize the conversion, to after read the same channel. This occurs when trying to update the value, and it updates them one after another. In all other cases buffered values are returned.\\


%----------------------------------------------------------------
\subsection{Alerts and Sensors Thresholds}

Sensor's values are monitored regarding their upper and lower thresholds. The \gls{ATCA} specification defines three thresholds for both upper and lower values. Those thresholds are used in an hysteresis way, that defines the assertion and de-assertion of the different alerts as illustrated in \textit{Figure \ref{alert_hysteresis_system}}.\\

\begin{center}
\begin{figure}[!ht]
	\includegraphics[scale=0.35]{src/LEGACY/img/alert_hysteresis_system.jpeg}
	\caption{Alert Hysteresis System \cite{picmg_atca_specs}}
	\label{alert_hysteresis_system}
\end{figure}
\end{center}

\textbf{->} IPMI \textbf{non-critical} or \textbf{Minor alert}: a warning meaning things are somewhat out of normal range, but not really a "problem" yet:
	\begin{itemize}
		\item upper-non-critical \textit{(Upper-NC)}
		\item lower-non-critical \textit{(Lower-NC)}
	\end{itemize}
	
\begin{figure}
	\begin{center}
	\includegraphics[scale=0.4]{src/LEGACY/img/shm_minor_alert.png}
	\end{center}
	\caption{Shelf Manager Minor Alert \cite{mann_schroff_shm_cooling}}
	\label{shm_minor_alert}
\end{figure}

\noindent In this case, the Shelf Manager only progressively increases the fan level regarding the temperature value \textit{(Figure \ref{shm_minor_alert})}.\\\\


\textbf{->} IPMI \textbf{critical} or \textbf{Major alert}: things are still in valid operating range, but are getting close to the edge; unit still operating within vendor-specified tolerances:
	\begin{itemize}
		\item upper-critical \textit{(Upper-C)}
		\item lower-critical \textit{(Lower-C)}
	\end{itemize}
	
\begin{figure}
	\begin{center}
	\includegraphics[scale=0.4]{src/LEGACY/img/shm_major_alert.png}
	\end{center}
	\caption{Shelf Manager Major Alert \cite{mann_schroff_shm_cooling}}
	\label{shm_major_alert}
\end{figure}

\noindent In this case, the Shelf Manager sets the fan level at its maximum \textit{(15)}, then decreases it progressively regarding the temperature value \textit{(Figure \ref{shm_major_alert})}.\\\\


\textbf{->} IPMI \textbf{non-recoverable} or \textbf{Critical alert}: unit no longer operating within vendor-specified tolerances:
	\begin{itemize}
		\item upper-non-recoverable \textit{(Upper-NR)}
		\item lower-non-recoverable	\textit{(Lower-NR)}
	\end{itemize}

\begin{figure}
	\begin{center}
	\includegraphics[scale=0.4]{src/LEGACY/img/shm_critical_alert.png}
	\end{center}
	\caption{Shelf Manager Critical Alert \cite{mann_schroff_shm_cooling}}
	\label{shm_critical_alert}
\end{figure}

\noindent In this case, the Shelf Manager sets the fan level at its maximum level, ie. 15 (\textit{Figure \ref{shm_critical_alert}}), and can power-down the board if it has been configured to do it \textit{(PEF global action)}.\\\\

When an alert occurs, the shelf manager can also negotiate new power-load values if this case is handled by the board. However, in all cases the upper level (for instance a human supervisor) is notified, and it is up to the supervisor to clear the alert, take the right decisions and operate in the most convenient way.


%----------------------------------------------------------------
\subsection{LATOME Sensors Thresholds}\label{latome_sensors_thesholds_ss}

The list of the sensors available on the \gls{LATOME} board is shown in \textit{Table \ref{latome_sensors_list}}. They are sorted into three categories:
\begin{enumerate}
	\item High-Priority \textit{(HP)} sensors: all used in the production version, and always enabled.
	\item Low-Priority \textit{(LP)} sensors: can also be used in the production version - but to limit memory usage, they are only enabled if required in the configuration at compile-time.
	\item Test \textit{(TEST)} sensors: added on demand at compile-time when memory usage and \gls{IPMB} congestion is not a constraint (ie. when debugging few \gls{LATOME} boards in the shelf).
\end{enumerate}

\begin{sidewaysfigure}
	\begin{center}
	\includegraphics[scale=0.70]{src/LEGACY/img/ipmi_thresholds_generator.pdf}
	\end{center}
	\caption{IPMI Thresholds Generator}
	\label{ipmi_thresholds_generator}
\end{sidewaysfigure}

Those sensors and their respective thresholds are described in a header file in the \gls{MMC} firmware repository \textit{(./MMC/user/sdr/)}. There is one header file per \gls{LATOME} version \textit{(V1 or V2)} and per production version \textit{(HP, LP or TEST)}. Those files can be generated using the provided \textbf{IPMI\_ThresholdsGenerator} Python tool.\\

This Python script lies in \textit{Tools/Sensor\_Configs/IPMI\_ThresholdsGenerator} in the Git repository. Each sensor's name and threshold values are listed in \textit{IPMI\_SensorList.py} \textit{(See Figure \ref{ipmi_thresholds_generator})}. The main program is located in \textit{src/IPMI\_ThresholdsGenerator.py}. Generated files can be found in the \textit{'generated'} directory.\\

\begin{table}[!h]
\begin{center}
\begin{tabular}{|c|c|c|}
\hline
\rowcolor{Black}
\color{White}\textbf{Sensor}	& \color{White}\textbf{Priority}	& \color{White}\textbf{Description}\\
\hline
\rowcolor{Black}\multicolumn{3}{|c|}{\color{White}\textbf{Temperature \textit{(°C)}}}\\
\hline\rowcolor{SkyBlue}
TEMP\_FPGA			& HP	& FPGA temperature\\
\hline
TEMP\_UPOD\_LEFT	& TEST	& Left $\mu$POD temperature\\
\hline
TEMP\_UPOD\_RIGHT	& TEST	& Right $\mu$POD temperature\\
\hline\rowcolor{Apricot}
TEMP\_VCC\_DC\_DC	& LP	& DC/DC Converter temperature\\
\hline
\rowcolor{Black}\multicolumn{3}{|c|}{\color{White}\textbf{Voltage \textit{(V)}}}\\
\hline\rowcolor{SkyBlue}
VCC\_GXB Voltage	& HP	& VCC GXB voltage\\
\hline\rowcolor{SkyBlue}
A10\_VCC Voltage	& HP	& Arria10 VCC voltage\\
\hline
P3V3 Voltage 		& TEST 	& 3.3V\\
\hline
VCC\_RAM Voltage 	& TEST	& VCC RAM voltage\\
\hline
P1V5 Voltage 		& TEST 	& 1.5V\\
\hline
P1V8 Voltage		& TEST	& 1.8V\\
\hline
P2V5 Voltage		& TEST	& 2.5V\\
\hline
VCCA\_PLL Voltage	& TEST	& VCCA PLL voltage\\
\hline
\rowcolor{Black}\multicolumn{3}{|c|}{\color{White}\textbf{Current \textit{(A)}}}\\
\hline\rowcolor{SkyBlue}
VCC\_GXB Current	& HP 	& VCC GXB current\\
\hline\rowcolor{SkyBlue}
A10\_VCC Current	& HP	& Arria10 VCC current\\
\hline\rowcolor{Apricot}
P3V3 Current		& LP	& 3.3V current\\
\hline\rowcolor{Apricot}
VCC\_RAM Current	& LP	& Arria10 VCC RAM current\\
\hline
P1V5 Current		& TEST 	& 1.5V current\\
\hline
P1V8 Current		& TEST	& 1.8V current\\
\hline\rowcolor{Apricot}
P2V5 Current		& LP	& 2.5V current\\
\hline\rowcolor{Apricot}
VCCA\_PLL Current	& LP	& VCCA PLL current\\
\hline
\end{tabular}
\end{center}
\caption{\gls{LATOME} Sensors List}
\label{latome_sensors_list}
\end{table}


The temperature flags are detailed in \textit{Table \ref{latome_v1_table_temp_thresholds}}.

\begin{table}[!h]
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|}
	\rowcolor{Black}
   & & \multicolumn{3}{c}{\color{White}\textbf{Lower}} & \multicolumn{3}{c}{\color{White}\textbf{Upper}}\\
	\rowcolor{Black}
   \multirow{-2}{*}{\color{White}\textbf{Temperature Name}} & \multirow{-2}{*}{\color{White}\textbf{Register}} & \color{White}\textbf{NR} & \color{White}\textbf{C} & \color{White}\textbf{NC} & \color{White}\textbf{NC} & \color{White}\textbf{C} & \color{White}\textbf{NR}\\
\hline
TEMP\_FPGA	& Temp 1 MSB unsigned	& 0 & 1 & 2	& 75 & 80 & 85\\
\hline
TEMP\_UPOD\_LEFT	& Temp 2 MSB unsigned	& 0 & 1 &	2 &	50 & 55 & 60\\
\hline
TEMP\_UPOD\_RIGHT	& Temp 3 MSB unsigned	& 0 & 1 &	2 &	55 & 60 & 65\\
\hline
TEMP\_VCC\_DC\_DC	& Temp 4 MSB unsigned	& 0 & 1	& 2 & 50 & 55 & 60\\
\hline
\end{tabular}
\end{center}
\caption{\gls{LATOME} V1 Temperature thresholds (in °C)}
\label{latome_v1_table_temp_thresholds}
\end{table}
\vspace{2em}


%----------------------------------------------------------------
\subsubsection{LATOME V1 Thresholds}

\textit{Table \ref{latome_v1_table_voltage_thresholds}} depicts the use of the different voltage channels of the LTC2495 and their respective threshold values.

\begin{table}[!h]
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|c|}
	\rowcolor{Black}
   & & \multicolumn{3}{c}{\color{White}\textbf{Lower}} & \color{White}\textbf{Nominal} & \multicolumn{3}{c}{\color{White}\textbf{Upper}}\\
	\rowcolor{Black}
   \multirow{-2}{*}{\color{White}\textbf{Voltage Name}} & \multirow{-2}{*}{\color{White}\textbf{Channel}} & \color{White}\textbf{NR} & \color{White}\textbf{C} & \color{White}\textbf{NC} & & \color{White}\textbf{NC} & \color{White}\textbf{C} & \color{White}\textbf{NR}\\
\hline
VCC\_GXB Voltage	& CH1	& 0.90 & 0.95	& 1.00	& 1.03	& 1.20	& 1.30	& 1.35\\
\hline
A10\_VCC Voltage	& CH3	& 0.85	& 0.90	& 0.92	& 0.95	& 1.10	& 1.15	& 1.20\\
\hline
P3V3 Voltage	& CH5	& 3.05	& 3.15	& 3.20	& 3.30	& 3.40	& 3.45	& 3.50\\
\hline
VCC\_RAM Voltage	& CH7	& 0.85	& 0.90	& 0.92	& 0.95	& 0.98	& 1.00	& 1.05\\
\hline
P1V5 Voltage	& CH9	& 1.32	& 1.37	& 1.42	& 1.50	& 1.58	& 1.63	& 1.68\\
\hline
P1V8 Voltage	& CH11	& 1.61	& 1.66	& 1.71	& 1.80	& 1.89	& 1.94	& 1.99\\
\hline
P2V5 Voltage	& CH13	& 2.27	& 2.32	& 2.37	& 2.50	& 2.62	& 2.67	& 2.72\\
\hline
VCCA\_PLL Voltage	& CH15	& 1.61	& 1.66	& 1.71	& 1.80	& 1.89	& 1.94	& 1.99\\
\hline
\end{tabular}
\end{center}
\caption{\gls{LATOME} V1 Voltage thresholds (in V)}
\label{latome_v1_table_voltage_thresholds}
\end{table}

The COM pin is connected to 1.8V and not $3.3 \over 2$ \textit{(the schematic is outdated on this point)}.
\textit{Table \ref{latome_v1_table_current_thresholds}} depicts the use of the different current channels (differential voltage) of the LTC2495 and their respective minimum and maximum values:

\begin{table}[!h]
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|}
	\rowcolor{Black}
   & & \multicolumn{3}{c}{\color{White}\textbf{Lower}} & \multicolumn{3}{c}{\color{White}\textbf{Upper}}\\
	\rowcolor{Black}
   \multirow{-2}{*}{\color{White}\textbf{Current Name}} & \multirow{-2}{*}{\color{White}\textbf{Channels}} & \color{White}\textbf{NR} & \color{White}\textbf{C} & \color{White}\textbf{NC} & \color{White}\textbf{NC} & \color{White}\textbf{C} & \color{White}\textbf{NR}\\
\hline
VCC\_GXB Current	& CH0 - CH1	& 0 & 0.5	& 1	& 12	& 15 & 18\\
\hline
A10\_VCC Current	& CH2 - CH3	& 0 & 1 & 3 & 20	& 25 & 28\\
\hline
P3V3 Current	& CH4 - CH5	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
VCC\_RAM Current	& CH6 - CH7	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
P1V5 Current	& CH8 - CH9	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
P1V8 Current	& CH10 - CH11	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
P2V5 Current	& CH12 - CH13	& 0	& 0.1 & 0.5 & 5 & 7 & 8\\
\hline
A10\_VCCA\_PLL Current	& CH14 - CH15	& 0 & 0.1 & 0.5	& 5 & 7 & 8\\
\hline
\end{tabular}
\end{center}
\caption{\gls{LATOME} V1 Current thresholds (in A)}
\label{latome_v1_table_current_thresholds}
\end{table}


%----------------------------------------------------------------
\subsubsection{LATOME V2 Thresholds}

\textit{Table \ref{latome_v2_table_voltage_thresholds}} depicts the use of the different voltage channels of the LTC2495 and their respective threshold values.\\

\begin{table}[!h]
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|c|}
	\rowcolor{Black}
   & & \multicolumn{3}{c}{\color{White}\textbf{Lower}} & \color{White}\textbf{Nominal} & \multicolumn{3}{c}{\color{White}\textbf{Upper}}\\
	\rowcolor{Black}
   \multirow{-2}{*}{\color{White}\textbf{Voltage Name}} & \multirow{-2}{*}{\color{White}\textbf{Channel}} & \color{White}\textbf{NR} & \color{White}\textbf{C} & \color{White}\textbf{NC} & & \color{White}\textbf{NC} & \color{White}\textbf{C} & \color{White}\textbf{NR}\\
\hline
VCC\_GXB Voltage	& CH1	& 0.90 & 0.95	& 1.00	& 1.03	& 1.20	& 1.30	& 1.35\\
\hline
A10\_VCC Voltage	& CH3	& 0.80	& 0.85	& 0.87	& 0.90	& 1.05	& 1.10	& 1.15\\
\hline
P3V3 Voltage	& CH5	& 3.05	& 3.15	& 3.20	& 3.30	& 3.40	& 3.45	& 3.50\\
\hline
VCC\_RAM Voltage	& CH7	& 0.80	& 0.85	& 0.87	& 0.90	& 0.93	& 0.95	& 1.00\\
\hline
P1V5 Voltage	& CH9	& 1.32	& 1.37	& 1.42	& 1.50	& 1.58	& 1.63	& 1.68\\
\hline
P1V8 Voltage	& CH11	& 1.61	& 1.66	& 1.71	& 1.80	& 1.89	& 1.94	& 1.99\\
\hline
P2V5 Voltage	& CH13	& 2.27	& 2.32	& 2.37	& 2.50	& 2.62	& 2.67	& 2.72\\
\hline
VCCA\_PLL Voltage	& CH15	& 1.61	& 1.66	& 1.71	& 1.80	& 1.89	& 1.94	& 1.99\\
\hline
\end{tabular}
\end{center}
\caption{\gls{LATOME} V2 Voltage thresholds (in V)}
\label{latome_v2_table_voltage_thresholds}
\end{table}

The COM pin is connected to 1.8V and not $3.3 \over 2$ \textit{(the schematic is outdated on this point)}.
\textit{Table \ref{latome_v2_table_current_thresholds}} depicts the use of the different current channels (differential voltage) of the LTC2495 and their respective minimum and maximum values:

\begin{table}[!h]
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|}
	\rowcolor{Black}
   & & \multicolumn{3}{c}{\color{White}\textbf{Lower}} & \multicolumn{3}{c}{\color{White}\textbf{Upper}}\\
	\rowcolor{Black}
   \multirow{-2}{*}{\color{White}\textbf{Current Name}} & \multirow{-2}{*}{\color{White}\textbf{Channels}} & \color{White}\textbf{NR} & \color{White}\textbf{C} & \color{White}\textbf{NC} & \color{White}\textbf{NC} & \color{White}\textbf{C} & \color{White}\textbf{NR}\\
\hline
VCC\_GXB Current	& CH0 - CH1	& 0 & 0.5	& 1	& 12	& 15 & 18\\
\hline
A10\_VCC Current	& CH2 - CH3	& 0 & 1 & 3 & 20	& 25 & 28\\
\hline
P3V3 Current	& CH4 - CH5	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
VCC\_RAM Current	& CH6 - CH7	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
P1V5 Current	& CH8 - CH9	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
P1V8 Current	& CH10 - CH11	& 0	& 0.1 & 0.5 & 2 & 3 & 4\\
\hline
P2V5 Current	& CH12 - CH13	& 0	& 0.1 & 0.5 & 5 & 7 & 8\\
\hline
VCCA\_PLL Current	& CH14 - CH15	& 0 & 0.1 & 0.5	& 5 & 7 & 8\\
\hline
\end{tabular}
\end{center}
\caption{\gls{LATOME} V2 Current thresholds (in A)}
\label{latome_v2_table_current_thresholds}
\end{table}

\vspace{2em}
The temperature flags are detailed in \textit{Table \ref{latome_v2_table_temp_thresholds}}.\\

\begin{table}[!h]
\begin{center}
\begin{tabular}{|l|c|c|c|c|c|c|c|}
	\rowcolor{Black}
   & & \multicolumn{3}{c}{\color{White}\textbf{Lower}} & \multicolumn{3}{c}{\color{White}\textbf{Upper}}\\
	\rowcolor{Black}
   \multirow{-2}{*}{\color{White}\textbf{Temperature Name}} & \multirow{-2}{*}{\color{White}\textbf{Register}} & \color{White}\textbf{NR} & \color{White}\textbf{C} & \color{White}\textbf{NC} & \color{White}\textbf{NC} & \color{White}\textbf{C} & \color{White}\textbf{NR}\\
\hline
TEMP\_FPGA	& Temp 1 MSB unsigned	& 0 & 1 & 2	& 75 & 80 & 85\\
\hline
TEMP\_UPOD\_LEFT	& Temp 2 MSB unsigned	& 0 & 1 &	2 &	50 & 55 & 60\\
\hline
TEMP\_UPOD\_RIGHT	& Temp 3 MSB unsigned	& 0 & 1 &	2 &	55 & 60 & 65\\
\hline
TEMP\_VCC\_DC\_DC	& Temp 4 MSB unsigned	& 0 & 1	& 2 & 50 & 55 & 60\\
\hline
\end{tabular}
\end{center}
\caption{\gls{LATOME} V2 Temperature thresholds (in °C)}
\label{latome_v2_table_temp_thresholds}
\end{table}


%----------------------------------------------------------------
\subsection{Sensors Data Conversion}

Data related to the board's sensors are stored into the \gls{SDR}. For example, a temperature sensor will have an entry in this database. In this entry, we will find information about the sensor, ie. its name, the data format and also the value of the sensor. This value is encoded on 8-bits.
As a sensor resolution is generally greater than 8-bits (ie. 11-bits, 12-bits or even 16-bits), a formula is defined in the specification to retrieve the real value of the sensor from the reduced 8-bits value:\\

$y = (M * x + B * 10^{Bexp}) * 10^{Rexp}$\\

Our goal is to select a range and the function parameters which would permit us to loose the minimum of information during the conversion from N-bits to 8-bits, and to come out with a consistent value that fits the application requirements (ie. the events that we want to monitore must be expressed within the 8-bits reduced sensor range).

\begin{enumerate}
	\item Temperature sensor
	
Device: LM95234\\

In order to convert the 11-bits value of the temperature sensor to an 8-bits value, we need to define a reduced range and to specify the format and formula parameters allowing to retrieve the real value of the sensor. In our case we only keep the 8 MSB bits:
\begin{itemize}
	\item Range: 0 °C $\rightarrow$ 255 °C
	\item Step: +1.0 °C
	\item Format: unsigned 8-bits
	\item From the 11-bits unsigned conversion result, we keep the bits [12:5]
	\item Formula parameters:
	\begin{itemize}
		\item M = 1
		\item B = 0
		\item Bexp = 0
		\item Rexp = 0
	\end{itemize}
\end{itemize}
	
	\item Voltage sensor

Device: LTC2495\\

In order to convert the 16-bits value of the voltage sensor to an 8-bits value, we need to define a reduced range and to specify the format and formula parameters allowing to retrieve the real value of the sensor.

In this case, as the conversion result is expressed in 2's complement, we have to keep the MSB bit representing the sign of the voltage value:
\begin{itemize}
	\item Range: 0,150 V $\rightarrow$ 3.450 V
	\item Step: +0.013 V
	\item Format: 2's complement 8-bits
	\item From the 24-bits conversion result, we keep the bits [22:15]
	\item Formula parameters:
	\begin{itemize}
		\item M = 13
		\item B = 15
		\item Bexp = 1
		\item Rexp = -3
	\end{itemize}
\end{itemize}
	
	\item Current sensor
	
	Device: LTC2495\\
	
In order to convert the 16-bits value of the current sensor to an 8-bits value, we need to define a reduced range and to specify the format and formula parameters allowing to retrieve the real value of the sensor.

In this case, as the conversion result is expressed in 2's complement and \textbf{we supposed that the value should be positive}. We have to convert it into an unsigned value and to keep the significant bits that contains the sensor value:
\begin {itemize}
	\item Range: 0 A $\rightarrow$ 51.0 A
	\item Step: +0.200 A
	\item Format: 2's complement 8-bits
	\item From the 16-bits register values, we keep the bits [10:3] ([15:8] in the conversion result)
	\item Formula parameters:
	\begin{itemize}
		\item M = 200
		\item B = 0
		\item Bexp = 0
		\item Rexp = -3
	\end{itemize}
\end{itemize}

	\item 12V Payload

The 12V payload is measured by the internal ADC of the ATMega128 on the port PF0. The formula parameters to compute the payload value is the following:

\begin{itemize}
	\item $M = {V_{ref} \over resolution} * {(R_{top} + R_{bottom}) \over R_{top}} =  {3,3 \over 256} * {(49,9 + 150) \over 49,9} ~= 52*10^{-3} (= 0x34)$
	\item B = 0
	\item Bexp = 0
	\item Rexp = -3
\end{itemize}

Vref is the reference voltage of the board. 256 is the ADC resolution (10-bits truncated to 8-bits – we keep only the MSBs). The PF0 port is connected to the 12V via a voltage divider in which the top resistor equals 150 ohm, and the bottom one 49.9 ohm.


\end{enumerate}


%----------------------------------------------------------------
\subsection{SDR Values: Experiments on the Shelf}

The previously defined \gls{SDR} parameters have been tested on the \gls{LATOME} board (V1) plugged in the \gls{ATCA} shelf. The \gls{SDR} values computed by the shelf are compared with the expected ones in \textit{Table \ref{table_sdr_values}}.\\

\begin{table}
\begin{center}
\begin{tabular}{|c|c|c|c|c|}
\hline
\rowcolor{Black}
& \multicolumn{2}{c}{\color{White}\textbf{LATOME V1 Full}}	& \multicolumn{2}{c}{\color{White}\textbf{LATOME V2 Bare}}\\
\hline
\rowcolor{Black}
\color{White}\textbf{Sensor}	& \color{White}\textbf{Expected value}	& \color{White}\textbf{SDR value} & \color{White}\textbf{Expected value}	& \color{White}\textbf{SDR value}\\
\hline
\rowcolor{lightgray}\multicolumn{5}{|c|}{\textbf{Temperature \textit{(°C)}}}\\
\hline
TEMP\_FPGA	& 45.0	& 49.0 & 27.0 & 27.0\\
\hline
TEMP\_UPOD\_LEFT	& 40.0	& 40.0 & 27.0 & 26.0\\
\hline
TEMP\_UPOD\_RIGHT	& 40.0	& 43.0 & 27.0 & 27.0\\
\hline
TEMP\_VCC\_DC\_DC	& 35.0	& 38.0 & 27.0 & 31.0\\
\hline
\rowcolor{lightgray}\multicolumn{5}{|c|}{\textbf{Voltage \textit{(V)}}}\\
\hline
VCC\_GXB Voltage	& 1.030	& 1.060 & 1.030 & 1.047\\
\hline
A10\_VCC Voltage	& 0.950	& 0.969 & 0.900 & 0.891\\
\hline
P3V3 Voltage	& 3.300	& 3.309 & 3.300 & 3.309\\
\hline
VCC\_RAM Voltage	& 0.950	& 0.956 & 0.900 & 0.904\\
\hline
P1V5 Voltage	& 1.500	& 1.502 & 1.500 & 1.502\\
\hline
P1V8 Voltage	& 1.800	& 1.801 & 1.800 & 1.801\\
\hline
P2V5 Voltage	& 2.500	& 2.477 & 2.500 & 2.490\\
\hline
VCCA\_PLL Voltage	& 1.800	& 1.788 & 1.800 & 1.801\\
\hline
\rowcolor{lightgray}\multicolumn{5}{|c|}{\textbf{Current \textit{(A)}}}\\
\hline
VCC\_GXB Current	& 1.000	& 1.500 & 0.600 & 0.500\\
\hline
A10\_VCC Current	& 5.000	& 6.100 & 0.000 & 0.100\\
\hline
P3V3 Current	& 0.000	& 0.100 & 0.000 & 0.100\\
\hline
VCC\_RAM Current	& 0.000	& 0.100 & 0.000 & 0.100\\
\hline
P1V5 Current	& 0.000	& 0.100 & 0.000 & 0.100\\
\hline
P1V8 Current	& 0.000	& 0.100 & 0.000 & 0.100\\
\hline
P2V5 Current	& 1.500	& 1.500 & 0.300 & 0.300\\
\hline
VCCA\_PLL Current	& 1.200	& 1.200 & 0.000 & 0.100\\
\hline
\end{tabular}
\end{center}
\caption{Expected and measured \gls{SDR} values}
\label{table_sdr_values}
\end{table}
