REM ----------------------------------------------------------------
REM
REM 	Makfile to compile the ICARE Manual
REM
REM ----------------------------------------------------------------

REM Main file name
set TEX_FILE=icare_manual

@echo off

cls


REM ----------------------------------------------------------------
REM 	Handle script options
REM ----------------------------------------------------------------
if "%1"=="" (
	REM If no parameters, print help
	call:print_usage
) else (
	REM ----------------------------------------------------------------
	REM Create PDF file and store it into the pdf folder
	REM
	if "%1"=="pdf" (
		echo Create PDF file...
		pdflatex %TEX_FILE%.tex
		makeglossaries %TEX_FILE%.tex
		makeindex.exe -s %TEX_FILE%.ist -t %TEX_FILE%.glg -o %TEX_FILE%.gls %TEX_FILE%.glo
		bibtex %TEX_FILE%
		pdflatex %TEX_FILE%.tex
		pdflatex %TEX_FILE%.tex
		copy %TEX_FILE%.pdf .\pdf\%TEX_FILE%.pdf
	)
	REM ----------------------------------------------------------------
	REM Clean the directory deleting temporary files
	REM
	if "%1"=="clean" (
		echo Clean directory...
		call:clean_dir
	)
	if "%1"=="cleanall" (
		echo Clean all directories...
		call:clean_dir
		del pdf\%TEX_FILE%.pdf
	)
)

pause&goto:eof


REM ----------------------------------------------------------------
REM Print the help
REM
:print_usage
	echo Usage:
	echo   $ make pdf : Create the PDF file and the bibliography
	echo   $ make clean : Clean the repository"
	echo   $ make cleanall : Clean the repository and REMove the PDF output file
goto:eof


REM ----------------------------------------------------------------
REM Clean the directory
REM
:clean_dir
	del .\%TEX_FILE%.pdf .\*.aux .\*.bbl .\*.blg .\*.brf .\*.glg .\*.glo .\*.gls .\*.ist .\*.log .\*.maf .\*.mtc* .\*.out .\*.toc .\*.*~ 2>nul
goto:eof
